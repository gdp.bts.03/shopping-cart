package com.java.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer productId;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @OneToMany(mappedBy = "product", cascade=CascadeType.ALL)
    private List<ShoppingCartProduct> cartProducts;

//    @OneToMany(mappedBy = "product", cascade=CascadeType.ALL,fetch = FetchType.EAGER)
//    private List<MultipartFile> imageProduct;

    public Product() {
    }

    public Product(Integer productId, String name, Integer price) {
        this.productId = productId;
        this.name = name;
        this.price = price;
    }



    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }


    //    public List<MultipartFile> getImageProduct() {
//        return imageProduct;
//    }
//
//    public void setImageProduct(List<MultipartFile> imageProduct) {
//        this.imageProduct = imageProduct;
//    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((productId == null) ? 0 : productId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Product other = (Product) obj;
        if (productId == null) {
            if (other.productId != null)
                return false;
        } else if (!productId.equals(other.productId))
            return false;
        return true;
    }
}




