package com.java.spring.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

import java.util.List;


@Entity
@Table(name = "shoppingCart")
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer shoppingCartId;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(name = "dateCreated")
    private LocalDate dateCreated;

    @Column(name = "totalCartPrice")
    private double totalCartPrice;

    @OneToMany(mappedBy = "shoppingCart", cascade = CascadeType.ALL)
//    @JsonIgnore
    private List<ShoppingCartProduct> carts;

    public ShoppingCart() {
    }

    public ShoppingCart(Integer shoppingCartId, LocalDate dateCreated, List<ShoppingCartProduct> carts) {
        this.shoppingCartId = shoppingCartId;
        this.dateCreated = dateCreated;
        this.carts = carts;
    }


    public Integer getShoppingCartId() {
        return shoppingCartId;
    }

    public void setShoppingCartId(Integer shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }


    public List<ShoppingCartProduct> getCarts() {
        return carts;
    }

    public void setCarts(List<ShoppingCartProduct> carts) {
        this.carts = carts;
    }


    public double getTotalCartPrice() {
//
//        List<ShoppingCartProduct> cartProducts = getCarts();
//        for (ShoppingCartProduct cp : cartProducts) {
//            totalCartPrice += cp.getTotalPrice();
//        }
        return totalCartPrice;
}

    public void setTotalCartPrice(double totalCartPrice) {

        this.totalCartPrice = totalCartPrice;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((shoppingCartId == null) ? 0 : shoppingCartId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShoppingCart other = (ShoppingCart) obj;
        if (shoppingCartId == null) {
            if (other.shoppingCartId != null)
                return false;
        } else if (!shoppingCartId.equals(other.shoppingCartId))
            return false;
        return true;
    }

}
