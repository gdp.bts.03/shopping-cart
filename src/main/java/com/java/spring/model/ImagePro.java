//package com.java.spring.model;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import org.hibernate.annotations.GenericGenerator;
//
//import javax.persistence.*;
//import java.sql.Blob;
//
//@Entity
//@Table(name = "imageProduct")
//public class ImageProduct {
//
//    @Id
//    @GeneratedValue(generator = "uuid")
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
//    private String id;
//
//    private String fileName;
//
//    private String fileType;
//
//    @Lob
//    private byte[] data;
//
//    public ImageProduct() {
//
//    }
//
//    public ImageProduct(String fileName, String fileType, byte[] data) {
//        this.fileName = fileName;
//        this.fileType = fileType;
//        this.data = data;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public String getFileName() {
//        return fileName;
//    }
//
//    public String getFileType() {
//        return fileType;
//    }
//
//    public byte[] getData() {
//        return data;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public void setFileName(String fileName) {
//        this.fileName = fileName;
//    }
//
//    public void setFileType(String fileType) {
//        this.fileType = fileType;
//    }
//
//    public void setData(byte[] data) {
//        this.data = data;
//    }
//
////
////    @EmbeddedId
////    @JsonIgnore
////    private ImageProductKey key;
//
////    @Id
////    @GeneratedValue(strategy = GenerationType.AUTO)
////    @Column(name = "id")
////    private Integer imageId;
////
////    @Lob
////    @Column(name = "imageUrl")
////    private String imageUrl;
//
////    @ManyToOne(cascade = CascadeType.ALL)
////    @MapsId("productId")
////    @JoinColumn(name = "productId", nullable = false)
////    @JsonIgnore
////    @Column(name = "productId")
////    private Integer product;
////
////    public ImageProduct() {
////
////    }
////
////    public ImageProduct(Integer imageId, String imageUrl, Integer product) {
////        this.imageId = imageId;
////        this.imageUrl = imageUrl;
////        this.product = product;
////    }
////
////    public Integer getImageId() {
////        return imageId;
////    }
////
////    public void setImageId(Integer imageId) {
////        this.imageId = imageId;
////    }
////
////    public String getImageUrl() {
////        return imageUrl;
////    }
////
////    public void setImageUrl(String imageUrl) {
////        this.imageUrl = imageUrl;
////    }
////
////    public Integer getProduct() {
////        return product;
////    }
////
////    public void setProduct(Integer product) {
////        this.product = product;
////    }
//
//
//
////    public void setKey(ImageProductKey key) {
////        this.key = key;
////    }
////
////    public void setProduct(Product product) {
////        this.product = product;
////    }
//
////    @Override
////    public int hashCode() {
////        final int prime = 31;
////        int result = 1;
////        result = prime * result + ((imageId == null) ? 0 : imageId.hashCode());
////        result = prime * result + ((imageUrl == null) ? 0 : imageUrl.hashCode());
////        result = prime * result + ((product == null) ? 0 : product.hashCode());
//////        result = prime * result + ((key == null) ? 0 : key.hashCode());
////        return result;
////    }
////
////    @Override
////    public boolean equals(Object obj) {
////        if (this == obj)
////            return true;
////        if (obj == null)
////            return false;
////        if (getClass() != obj.getClass())
////            return false;
////
////        ImageProduct other = (ImageProduct) obj;
////        if (imageId == null) {
////            if (other.imageId != null)
////                return false;
////        } else if (!imageId.equals(other.imageId))
////            return false;
////
////        if (imageUrl == null) {
////            if (other.imageUrl != null)
////                return false;
////        } else if (!imageUrl.equals(other.imageUrl))
////            return false;
////
////        if (product == null) {
////            if (other.product != null)
////                return false;
////        } else if (!product.equals(other.product))
////            return false;
////
//////        if (key == null) {
//////            if (other.key != null)
//////                return false;
//////        } else if (!key.equals(other.key))
//////            return false;
////
////        return true;
////    }
//
//}
