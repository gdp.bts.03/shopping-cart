package com.java.spring.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="image")
public class ImageProduct {


    @Id
    @GeneratedValue
    private Integer imageId;
//    private String photographer;
    private String path;
    private String fileName;
//    private String comments;


    public ImageProduct(){

    }
    public ImageProduct(String fileName,String path) {
        this.fileName = fileName;
        this.path = path;

    }

    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


}

