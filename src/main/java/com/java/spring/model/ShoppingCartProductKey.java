package com.java.spring.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class ShoppingCartProductKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "productId")
    private Integer productId;

    @Column(name = "shoppingCartId")
    private Integer shoppingCartId;

    public ShoppingCartProductKey(){}

    public ShoppingCartProductKey(Integer shoppingCartId,Integer productId) {
        this.shoppingCartId = shoppingCartId;
        this.productId = productId;

    }


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getShoppingCartId() {
        return shoppingCartId;
    }

    public void setShoppingCartId(Integer shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((shoppingCartId == null) ? 0 : shoppingCartId.hashCode());
        result = prime * result + ((productId == null) ? 0 : productId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShoppingCartProductKey other = (ShoppingCartProductKey) obj;
        if (productId == null) {
            if (other.productId != null)
                return false;
        } else if (!productId.equals(other.productId))
            return false;

        if (shoppingCartId == null) {
            if (other.shoppingCartId != null)
                return false;
        } else if (!shoppingCartId.equals(other.shoppingCartId))
            return false;
        return true;
    }
}


