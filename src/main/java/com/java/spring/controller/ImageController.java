package com.java.spring.controller;

import com.java.spring.model.Constants;
import com.java.spring.model.ImageProduct;

import com.java.spring.model.Product;
import com.java.spring.repository.ImageRepository;
import com.java.spring.service.ImageService;

import com.java.spring.service.TesService;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import javax.websocket.server.PathParam;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import org.springframework.core.io.Resource;


@RestController
@RequestMapping("/tes/")
public class ImageController {

    //    Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    ImageService imageService;

    @Autowired
    ImageRepository imageRepository;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public @NotNull Iterable<ImageProduct> listProduct() {
        return imageService.getAllImagess();
    }

    @PostMapping("/uploadImage")
    public String uploadImage(@RequestParam("imageFile") MultipartFile imageFile) {
        return imageService.uploadImage(imageFile);
    }

    @Autowired
    TesService tesService;

    @PostMapping("/uImages")
    public void uploadMultipartFile(@RequestParam("uploadfile") MultipartFile file, Model model) {
        try {
            tesService.store(file);
            model.addAttribute("message", "File uploaded successfully! -> filename = " + file.getOriginalFilename());
        } catch (Exception e) {
            model.addAttribute("message", "Fail! -> uploaded filename: " + file.getOriginalFilename());
        }
//        return tes;
    }

}
