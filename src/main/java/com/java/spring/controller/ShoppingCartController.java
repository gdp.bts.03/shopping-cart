package com.java.spring.controller;


import com.java.spring.dto.ShoppingCartProductDto;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import com.java.spring.repository.ProductRepository;
import com.java.spring.repository.ShoppingCartProductRepository;
import com.java.spring.repository.ShoppingCartRepository;
import com.java.spring.service.ShoppingCartProductService;
import com.java.spring.service.ShoppingCartService;
import com.java.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/shoppingCarts")
public class ShoppingCartController {

    @Autowired
    ProductService productService;
    @Autowired
    ShoppingCartService shoppingCartService;
    @Autowired
    ShoppingCartProductService shoppingCartProductService;
    @Autowired
    ShoppingCartProductRepository shoppingCartProductRepository;

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ShoppingCartRepository shoppingCartRepository;


    /*
     * BUAT CREATE SHOPPING CART ID
     * */
    @PostMapping("/create")
    public ResponseEntity<ShoppingCart> create() {

        ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart = shoppingCartService.create(shoppingCart);
        shoppingCartService.update(shoppingCart);

        return new ResponseEntity<>(shoppingCart, HttpStatus.CREATED);
    }

    /*
     * BUAT MENAMBAHKAN ISI CART BY SHOPPING CART ID
     * */
    //tes Dto
    @PostMapping("/add")
    @ResponseStatus(HttpStatus.OK)
    public ShoppingCartProductDto save(@RequestBody ShoppingCartProductDto shoppingCartProductDto) {
        return shoppingCartProductDto;
    }

    /*
     * BUAT MENAMPILKAN ALL SHOPPING CART
     * */
//    @GetMapping("/")
//    @ResponseStatus(HttpStatus.OK)
//    public @NotNull Iterable<ShoppingCartProduct> list() {
//        return shoppingCartProductService.getAllCarts();
//    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public @NotNull Iterable<ShoppingCart> listCart() {
        return shoppingCartService.getAllCarts();
    }


    /*
     * BUAT MENAMPILKAN SHOPPING CART BY ID
     * */
    @GetMapping("/{shoppingCartId}")
    public @ResponseBody
    ShoppingCart getShoppingCartById(@PathVariable(value = "shoppingCartId") Integer shoppingCartId) {
        return shoppingCartService.getShoppingCart(shoppingCartId);
    }


    @PostMapping("/add/{shoppingCartId}")
    public ResponseEntity<?> add(@RequestBody List<ShoppingCartProductDto> dto,
                                 @PathVariable(value = "shoppingCartId") Integer shoppingCartId) {
        shoppingCartProductService.addShoppingCartProduct(shoppingCartId, dto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*
     * UPDATE CART BY SHOPPING CART ID
     *  */
    @PutMapping("/update/{shoppingCartId}")
    public ResponseEntity<?> update(@RequestBody ShoppingCartProductDto dto,
                                    @PathVariable(value = "shoppingCartId") Integer shoppingCartId) {
        shoppingCartProductService.updateCart(shoppingCartId, dto);
        return new ResponseEntity<>(HttpStatus.OK);
    }




    /*
     * DELETE SHOPPING CART BY ID
     * */
//    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json", value = "/deleteShoppingCart/{id}")
//    public String delete(@PathVariable(name = "id") Integer id) {
//        String result = "";
//        if (id == null) {
//            result = "id " + id + "tidak ditemukan ";
//            return result;
//        }
//        result = "id " + id + " berhasil di hapus";
//        shoppingCartService.delete(id);
//        return result;
//    }

    /*
     * DELETE SEMUA ISI CART BY SHOPPING CART ID
     * */
//    @RequestMapping(value = "/removeCart/{shoppingCartId}", method = RequestMethod.DELETE)
//    @ResponseStatus(value = HttpStatus.NO_CONTENT)
//    public void clearCart(@PathVariable(value = "shoppingCartId") Integer shoppingCartId) {
//        ShoppingCart shoppingCart = shoppingCartService.getShoppingCart(shoppingCartId);
//        shoppingCartProductService.removeAllShoppingCartProducts(shoppingCart);
//    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Illegal request, please verify your payload")
    public void handleClientErrors(Exception ex) {

    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal Server Error")
    public void handleServerErrors(Exception ex) {

    }

}