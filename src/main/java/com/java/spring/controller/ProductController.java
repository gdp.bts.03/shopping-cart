package com.java.spring.controller;


import com.java.spring.model.Product;

import com.java.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.core.io.Resource;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

//    @Autowired
//    private ImageService imageService;

    /*
     * ADD PRODUCT
     * */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", value = "/add")
    public void add(@RequestBody Product product) {
        productService.addProduct(product);
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json", value = "/addAll")
    public void add(@RequestBody List<Product> product) {
        productService.addAllProduct(product);
    }

    /*
     * GET PRODUCT
     * */
//    @RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "/all")
//    public @NotNull Iterable<Product> getProducts() {
//        return productService.getAllProducts();
//    }
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public @NotNull Iterable<Product> listProduct() {
        return productService.getAllProducts();
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "get/{id}")
    public Product getProduct(@PathVariable(name = "id") Integer id) {
        return productService.getProduct(id);
    }

    //    @PutMapping(value = "/update/{id}")
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json", consumes = "application/json", value = "/update/{id}")
    public void update(@RequestBody Product product, @PathVariable Integer id) {
        productService.updateProduct(id, product);
    }

    //    @DeleteMapping(value = "/delete/{id}")
    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json", value = "/delete/{id}")
    public String delete(@PathVariable(name = "id") Integer id) {
        String result = "";
        if (id == null) {
            result = "id " + id + "tidak ditemukan ";
            return result;
        }
        result = "id " + id + " berhasil di hapus";
        productService.deleteProduct(id);
        return result;
    }





    /*
     * ADD IMAGE
     * */
//    @RequestMapping(value = "/upload",method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//    public ResponseEntity<Objects> uploadImage(@RequestParam("file") MultipartFile file) throws IOException {
//        File convertFile = new File("C:\\Users\\Public\\Pictures\\Sample Pictures"+file.getOriginalFilename());
//        convertFile.createNewFile();
//        FileOutputStream fout = new FileOutputStream(convertFile);
//        fout.write(file.getBytes());
//        fout.close();
//        return new ResponseEntity("File is uploaded successfully", HttpStatus.OK);
//    }



//    @PostMapping("/uploadFile")
//    public ImageDto uploadFile(@RequestParam("file") MultipartFile file) {
//        ImageProduct fileName = imageService.storeFile(file);
//
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/api/products/images/")
//                .path(fileName.getFileName())
//                .toUriString();
//        ImageDto dto = new ImageDto(fileName.getFileName(), fileDownloadUri,
//                file.getContentType(), file.getSize());
//
//        return dto;
//
//    }


//    @PostMapping("/uploadMultipleFiles")
//    public List <ImageDto> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//    }
//
//    @GetMapping("/images/{fileName:.+}")
//    public ResponseEntity < Resource > downloadFile(@PathVariable String fileName, HttpServletRequest request) {
//        // Load file as Resource
//        ImageProduct imageProduct = imageService.getFile(fileName);
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(imageProduct.getFileType()))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + imageProduct.getFileName() + "\"")
//                .body(new ByteArrayResource(imageProduct.getData()));
//    }








}
