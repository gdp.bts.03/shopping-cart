package com.java.spring.dto;

import com.java.spring.model.ShoppingCartProduct;

import java.io.Serializable;
import java.util.List;


public class ShoppingCartProductDto implements Serializable {


    private Integer productId;
    private Integer quantity;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}