package com.java.spring.service;


import com.java.spring.model.ImageProduct;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public interface ImageService {

    public List<ImageProduct> getAllImagess();

    public ImageProduct saveImage(ImageProduct imageProduct, MultipartFile imageFile) throws Exception;

    public void saveI(ImageProduct imageProduct);

    public void save(MultipartFile imageFile, ImageProduct imageProduct) throws Exception;

    public String uploadImage(MultipartFile imageFile);

//    void upload(MultipartFile file) throws Exception;
//
//    public String store(MultipartFile file);







}
