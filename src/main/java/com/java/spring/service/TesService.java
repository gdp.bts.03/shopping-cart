package com.java.spring.service;


import com.java.spring.exception.FileStorageException;
import com.java.spring.model.ImageProduct;
import com.java.spring.repository.ImageRepository;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Random;
import java.util.stream.Stream;

import java.io.IOException;
import java.net.MalformedURLException;

import org.springframework.core.io.UrlResource;

import org.springframework.util.FileSystemUtils;


@Service
public class TesService {
    Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final Path rootLocation = Paths.get("images");
    private final Path fileStorageLocation = Paths.get("images");

//    @Autowired
//    private Path fileStorageLocation;
    @Autowired
    ImageRepository imageRepository;

    public ImageProduct store(MultipartFile file) {
        ImageProduct imageProduct = new ImageProduct();
//        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExtension = ".jpeg";
        String fileName = Integer.toString(new Random().nextInt(1000000000)) + fileExtension;
        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
//            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));


            // These to track in database
            imageProduct.setFileName(fileName);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/images/")
                    .path(imageProduct.getFileName())
                    .toUriString();
            imageProduct.setPath(fileDownloadUri);

            imageRepository.save(imageProduct);
            return imageProduct;
//            return new ImageProduct(fileName,fileDownloadUri);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }

    }

    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error! -> message = " + e.getMessage());
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    public Stream<Path> loadFiles() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new RuntimeException("\"Failed to read stored file");
        }
    }

}
