package com.java.spring.service;

import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import org.springframework.stereotype.Repository;


import java.util.List;


public interface ProductService {



    /* get by id*/
    Product getProduct(Integer id);

    /* add product*/
    Product addProduct(Product product);
    public List<Product> addAllProduct(List<Product> product);


    /*get all*/
//    List<Product> getAllProducts();
    public Iterable<Product> getAllProducts();

    /* update product*/
    Product updateProduct(Integer id, Product product);

    /* delete product by id*/
    void deleteProduct(Integer id);
}
