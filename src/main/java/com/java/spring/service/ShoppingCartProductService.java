package com.java.spring.service;


import com.java.spring.dto.ShoppingCartProductDto;
import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

public interface ShoppingCartProductService {


    void totalCartPrice(ShoppingCart shoppingCart, Integer price, Integer quantity);

    //    void addShoppingCartProduct(Integer shoppingCartId,ShoppingCartProductDto dto);
    List<ShoppingCartProduct> addShoppingCartProduct(Integer shoppingCartId, List<ShoppingCartProductDto> dto);
//    public List<ShoppingCartProduct> addAll(Integer shoppingCartId, ShoppingCartProductDto dto);


    //    public ShoppingCartProduct getShoppingCartProduct(Integer shoppingCartId);
    Iterable<ShoppingCartProduct> getAllCarts();

    //    void removeShoppingCartProduct(ShoppingCartProduct shoppingCartProduct);
//    void removeAllShoppingCartProducts(ShoppingCart shoppingCart);
    void updateCart(Integer shoppingCartId, ShoppingCartProductDto dto);

//    void deleteCartById(ShoppingCartProduct shoppingCartProduct);
//    void deleteCart(ShoppingCart shoppingCart);


//    ShoppingCartProduct getShoppingCartProductByProductId(Long productId);


}

