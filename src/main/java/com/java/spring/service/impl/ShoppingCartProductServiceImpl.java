package com.java.spring.service.impl;


import com.java.spring.dto.ShoppingCartProductDto;
import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import com.java.spring.model.ShoppingCartProductKey;
import com.java.spring.repository.ProductRepository;
import com.java.spring.repository.ShoppingCartProductRepository;
import com.java.spring.repository.ShoppingCartRepository;
import com.java.spring.service.ProductService;
import com.java.spring.service.ShoppingCartProductService;
import com.java.spring.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class ShoppingCartProductServiceImpl implements ShoppingCartProductService {

    @Autowired
    private ShoppingCartProductRepository shoppingCartProductRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingCartService shoppingCartService;


    @Override
    public Iterable<ShoppingCartProduct> getAllCarts() {
        return this.shoppingCartProductRepository.findAll();
    }

    /*
     * ADD PRODUCT TO CART
     * */

    @Override
    public void totalCartPrice(ShoppingCart shoppingCart, Integer price, Integer quantity) {
        double totalCartPrice = 0;
        List<ShoppingCartProduct> cartProducts = shoppingCart.getCarts();
        for (ShoppingCartProduct cp : cartProducts) {
            totalCartPrice += cp.getTotalPrice() + (price * quantity);
        }
        shoppingCart.setTotalCartPrice(totalCartPrice);
        shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public List<ShoppingCartProduct> addShoppingCartProduct(Integer shoppingCartId, List<ShoppingCartProductDto> dto) {

        List<ShoppingCartProduct> cartProduct = new LinkedList<>();
        ShoppingCart shoppingCart = shoppingCartRepository.findById(shoppingCartId).get();

        for (ShoppingCartProductDto cart : dto) {
            Product product = productRepository.findById(cart.getProductId()).get();
            ShoppingCartProduct shoppingCartProduct = new ShoppingCartProduct();
            shoppingCartProduct.setKey(new ShoppingCartProductKey(shoppingCartId, cart.getProductId()));
            shoppingCartProduct.setShoppingCart(shoppingCart);
            shoppingCartProduct.setProduct(product);
            shoppingCartProduct.setQuantity(cart.getQuantity());
            shoppingCartProduct.setTotalPrice(product.getPrice() * cart.getQuantity());

            cartProduct.add(shoppingCartProduct);

        }
        return shoppingCartProductRepository.saveAll(cartProduct);
    }

    /*
     * UPDATE CART
     * */
    @Override
    public void updateCart(Integer shoppingCartId, ShoppingCartProductDto dto) {
        ShoppingCartProduct cartProduct = new ShoppingCartProduct();
        ShoppingCart shoppingCart = shoppingCartRepository.findById(shoppingCartId).get();
        Product product = productRepository.findById(dto.getProductId()).get();

        cartProduct.setKey(new ShoppingCartProductKey(shoppingCartId, dto.getProductId()));
        cartProduct.setShoppingCart(shoppingCart);
        cartProduct.setProduct(product);
        cartProduct.setQuantity(dto.getQuantity());

        cartProduct.setTotalPrice(product.getPrice() * dto.getQuantity());

        shoppingCartProductRepository.saveAndFlush(cartProduct);
    }



    /*
    * DELETE CART
    * */
//    @Override
//    public void deleteCartById(IntegerShoppingCartProduct shoppingCartProduct) {
//        shoppingCartProductRepository.deleteById(shoppingCartProduct);
//    }
//
//    @Override
//    public void deleteCart(Integer shoppingCartId, ShoppingCartProductDto dto) {
//        ShoppingCartProduct cartProduct = new ShoppingCartProduct();
//        ShoppingCart shoppingCart = shoppingCartRepository.findById(shoppingCartId).get();
//        Product product = productRepository.findById(dto.getProductId()).get();
//
//        cartProduct.setKey(new ShoppingCartProductKey(shoppingCartId, dto.getProductId()));
//        cartProduct.setShoppingCart(shoppingCart);
//        cartProduct.setProduct(product);
//        cartProduct.setQuantity(dto.getQuantity());
//
//        cartProduct.setTotalPrice(product.getPrice() * dto.getQuantity());
//
//        shoppingCartProductRepository.delete(cartProduct);
//    }



    /*
     * REMOVE ALL ITEM CART
     * */
//    @Override
//    public void removeShoppingCartProduct(ShoppingCartProduct shoppingCartProduct) {
//        shoppingCartProductRepository.delete(shoppingCartProduct);
//    }
//
//    @Override
//    public void removeAllShoppingCartProducts(ShoppingCart shoppingCart) {
//        List<ShoppingCartProduct> shoppingCartProducts = shoppingCart.getCarts();
//
//        for (ShoppingCartProduct item : shoppingCartProducts) {
//            removeShoppingCartProduct(item);
//        }
//
//    }

}
