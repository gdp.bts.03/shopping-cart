package com.java.spring.service.impl;


import com.java.spring.exception.FileStorageException;
import com.java.spring.model.Constants;
import com.java.spring.model.ImageProduct;
import com.java.spring.repository.ImageRepository;
import com.java.spring.service.ImageService;
import jdk.nashorn.internal.ir.RuntimeNode;
import org.apache.commons.io.FilenameUtils;
import org.omg.CORBA.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.xml.transform.stream.StreamResult;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class ImageServiceImpl implements ImageService {
    @Autowired
    ImageRepository imageRepository;

    @Autowired
    ServletContext context;
//

    //    private Path rootLocation = Paths.get("images");
    private final Path fileStorageLocation = Paths.get("images");

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<ImageProduct> getAllImagess() {
        return imageRepository.findAll();
    }


    public ImageProduct saveImage(ImageProduct imageProduct, MultipartFile file) throws Exception {
        String fileExtension = ".jpeg";
        String fileName = Integer.toString(new Random().nextInt(1000000000)) + fileExtension;

        // Copy file to the target location (Replacing existing file with the same name)
        Path targetLocation = this.fileStorageLocation.resolve(fileName);
        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

        // These to track in database
        imageProduct.setFileName(fileName);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/images/")
                .path(imageProduct.getFileName())
                .toUriString();
        imageProduct.setPath(fileDownloadUri);

        return imageRepository.save(imageProduct);
    }

    @Override
    public void saveI(ImageProduct imageProduct) {
        imageRepository.save(imageProduct);
    }

    @Override
    public void save(MultipartFile imageFile, ImageProduct imageProduct) throws Exception {
        saveImage(imageProduct, imageFile);
        saveI(imageProduct);
    }

    @Override
    public String uploadImage(MultipartFile imageFile) {
        String returnValue = "File uploaded successfully";

        ImageProduct imageProduct = new ImageProduct();
        imageProduct.setFileName(StringUtils.cleanPath(imageFile.getOriginalFilename()));
//        imageDto.setPath("/images/");

        try {
            save(imageFile, imageProduct);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.error("Error saving photo", e);
            returnValue = "error";
        }
        return returnValue;
    }

}
