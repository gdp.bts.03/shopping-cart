package com.java.spring.service.impl;

import com.java.spring.exception.ResourceNotFoundException;
import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import com.java.spring.repository.ProductRepository;
import com.java.spring.repository.ShoppingCartRepository;
import com.java.spring.service.ShoppingCartService;
import org.hibernate.query.criteria.internal.expression.function.AggregationFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@Component
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private ProductRepository productRepository;

    /*
     * CREATE
     * */
    @Override
    public ShoppingCart create(ShoppingCart shoppingCart) {

        shoppingCart.setDateCreated(LocalDate.now());

        return this.shoppingCartRepository.save(shoppingCart);
    }

    /*
     * GET ALL
     * */
    @Override
    public Iterable<ShoppingCart> getAllCarts() {
        return this.shoppingCartRepository.findAll();
    }

    /*
     * GET SHOPPING CART BY ID
     * */
    @Override
    public ShoppingCart getShoppingCart(Integer shoppingCartId) {
        return shoppingCartRepository
                .findById(shoppingCartId)
                .orElseThrow(() -> new ResourceNotFoundException("Product not found"));

    }

    @Override
    public void update(ShoppingCart shoppingCart) {
        this.shoppingCartRepository.save(shoppingCart);
    }

    /*
     * DELETE SHOPPING CART BY ID
     * */
    @Override
    public void delete(Integer id) {
        shoppingCartRepository.deleteById(id);
    }
}
