package com.java.spring.service.impl;

import com.java.spring.exception.ResourceNotFoundException;
import com.java.spring.model.Product;
import com.java.spring.repository.ProductRepository;
import com.java.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Component
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product addProduct(Product product) {
        return productRepository.save(product);
    }
    @Override
    public List<Product> addAllProduct(List<Product> product) {
        return productRepository.saveAll(product);
    }

    @Override
    public List<Product> getAllProducts() {
//    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }


    @Override
    public Product getProduct(Integer id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product not found"));
    }

    @Override
    public Product updateProduct(Integer id, Product product) {
       return productRepository.saveAndFlush(product);
    }

    @Override
    public void deleteProduct(Integer id) {
        productRepository.deleteById(id);
    }
}



